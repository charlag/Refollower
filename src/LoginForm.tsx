import { h } from "preact";
import { useState } from "preact/hooks";
import type { ObjectStorage } from "./ObjectStorage";

export function LoginForm(
    props: {
        onLogin: (instance: string) => void,
        objectStorage: ObjectStorage,
    }
) {
    const [value, setValue] = useState("");

    function onChange(event: Event) {
        const input = event.target as HTMLInputElement
        setValue(input.value);
    }

    function onSubmit(e: Event) {
        e.preventDefault()
        props.onLogin(value)
    }

    return (
        <div className="login">
            <h2>Refollower</h2>
            <form id="form" onSubmit={onSubmit}>
                <label>What's your server?</label>
                <div className="instance-picker">
                    {props.objectStorage.keys().map(instance => (
                        <button className="button" onClick={() => props.onLogin(instance)}>{instance}</button>))
                    }
                </div>
                <input type="text" className="input" value={value} onChange={onChange}>
                </input>
                <input type="submit" className="button" value="Login">
                </input>
            </form>
            <h3>
                What would happen now?
            </h3>
            <p>
                After you fill in your domain name and click login your server will ask you to authorize the app. After that
                the list of your followers will be loaded and you can view and manage moved and old accounts.
            </p>

            <h3>
                Privacy
            </h3>
            <p>
                Everything is stored and accessed from your browser. We do not get access to your account.
            </p>
        </div>
    )
}