import { h } from "preact";
import { useEffect, useReducer, useState } from "preact/hooks";
import { AccountInfo } from "./AccountInfo";
import type { Account } from "./ApiModel";
import type { Controller } from "./Controller";
import { DatePicker } from "./DatePicker";
import { ProgressIndicator } from "./ProgressIndicator";
import { showSnackbar } from "./Snackbar";

type Props = {
    controller: Controller,
}

type Action =
    | { type: "accountsLoaded", accounts: Array<Account> }
    | { type: "removed", accountId: number }

function accountsReducer(state: Array<Account>, action: Action) {
    switch (action.type) {
        case "accountsLoaded":
            return state.concat(action.accounts);
        case "removed":
            return state.filter(a => a.id !== action.accountId)
        default:
            throw new Error("Unknown action " + action);
    }
}

export function FollowerAccountList({ controller }: Props) {
    const [isLoading, setLoading] = useState(false);
    const [accounts, dispatch] = useReducer(accountsReducer, []);
    const [inactiveSince, setInactiveSince] = useState(() => {
        const d = new Date();
        d.setHours(0)
        d.setMinutes(0);
        d.setSeconds(0);
        d.setMilliseconds(0);
        return d;
    })

    useEffect(() => {
        setLoading(true);
        controller.fetchFollowers((batch) => dispatch({ type: "accountsLoaded", accounts: batch }))
            .finally(() => setLoading(false))
    }, []);

    async function onRemove(e: Event, account: Account) {
        e.stopPropagation();
        try {
            await controller.onRemoveFromFollowers(account);
            dispatch({ type: "removed", accountId: account.id });
            showSnackbar("Removed " + account.acct);
        } catch (e) {
            showSnackbar(e.messagge);
        }
    }

    return <div className="account-list">
        <ProgressIndicator loading={isLoading} />
        <h2>Followers inactive since {inactiveSince.toLocaleDateString()}:</h2>
        <div style="margin-bottom: 8px;">
            <DatePicker date={inactiveSince} onDateSelected={setInactiveSince}/>
        </div>
        {
            accounts.map(a => {
                const d = new Date(a.last_status_at);
                if (d > inactiveSince) {
                    return null;
                }
                return <div className="item" key={a.id}>
                    <AccountInfo account={a}>
                        <div className="following-items">
                            <div className="following-active-date">{a.last_status_at == null ? "--" : d.toLocaleDateString()}</div>
                            <button className="text-button" onClick={(e) => { onRemove(e, a) }}>Remove</button>
                        </div>
                    </AccountInfo>
                </div>
            })
        }
    </div>
}