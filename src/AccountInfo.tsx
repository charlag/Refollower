import { h } from "preact"
import type { RenderableProps } from "preact"
import type { Account } from "./ApiModel"
import type { Relationship } from "./Controller"

export function AccountInfo(props: RenderableProps<{ account: Account, relationship?: Relationship }>) {
    const a = props.account
    return <div className="account-info" onClick={() => window.open(a.url)}>
        {props.relationship ? relationshipElement(props.relationship) : null}
        <img className="avatar" src={a.avatar}></img>
        <div className="usernames">
            <div className="display-name">{a.username}</div>
            <div className="display-name">{a.acct}</div>
        </div>
        {props.children}
    </div>
}

function relationshipElement(relationship: Relationship) {
    let text, title
    if (relationship.following && relationship.followed_by) {
        text = "↔"
        title = "Mutual"
    } else if (relationship.requested) {
        text = "↻"
        title = "Requested"
    } else if (relationship.following) {
        text = "→"
        title = "Following"
    } else if (relationship.followed_by) {
        text = "←"
        title = "Followed by"
    } else {
        text = " "
        title = "Neutral"
    }
    return <div className="relationship-icon" title={title}>{text}</div>
}