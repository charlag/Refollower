import { h } from "preact"

export function ProgressIndicator({loading}: {loading: boolean}) {
    const style = !loading ? "opacity: 0" : "";
    return <div className="indefinite-progress" style={style}></div>;
}