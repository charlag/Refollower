
export function showSnackbar(text: string) {
    const element = document.createElement("div")
    element.className = "snackbar";

    element.innerText = text;

    document.body.appendChild(element);

    setTimeout(() => document.body.removeChild(element), 2000);
}