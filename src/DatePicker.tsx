import { h } from "preact"
import { useEffect, useRef } from "preact/hooks";

export function DatePicker({ date, onDateSelected }: { date: Date, onDateSelected: (date: Date) => void }) {
    const inputEl = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (inputEl.current) inputEl.current.valueAsDate = date
    }, []);
    const initInput = (input: HTMLInputElement | null) => {
        inputEl.current = input;
        if (input) input.valueAsDate = date;
    }

    return <div>
        <input type="date" ref={initInput}></input>
        <button
            style="margin: 0 8px;"
            className="text-button"
            onClick={() => inputEl.current?.valueAsDate && onDateSelected(inputEl.current.valueAsDate)}
        >
            Filter
        </button>
    </div>
}