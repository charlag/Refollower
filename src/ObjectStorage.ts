export class ObjectStorage {
	get(name: string): any | null {
		const s = localStorage.getItem(name)
		return s == null ? s : JSON.parse(s) 
	}

	set(name: string, value: any) {
		const s = JSON.stringify(value)
		localStorage.setItem(name, s)
	}

	keys(): Array<string> {
		return Object.keys(localStorage)
	}
}