
import { Account, ApiModel } from "./ApiModel";
import type { ObjectStorage } from "./ObjectStorage";

export type Relationship = {
	id: string,
	following: boolean,
	requested: boolean,
	followed_by: boolean,
	blocking: boolean,
	blocked_by: boolean,
}

export type RelatedAccount = {
	account: Account,
	relationship: Relationship,
}

export type AccountPair = {
	oldAccount: RelatedAccount,
	newAccount: RelatedAccount,
}

export class Controller {
	readonly apiModel: ApiModel;

	constructor(objectStorage: ObjectStorage) {
		this.apiModel = new ApiModel(objectStorage);
	}

	async handleOauthRedirect(): Promise<string | null> {
		const url = new URL(window.location.href);
		if (url.searchParams.has("code")) {
			const newUrl = new URL(window.location.href);
			const instance = newUrl.searchParams.get("instance");

			newUrl.searchParams.delete("code");
			newUrl.searchParams.delete("instance");
			window.history.pushState(null, "", newUrl.toString());
			await this.apiModel.fetchOauthToken(url);
			return instance;
		} else {
			return null;
		}
	}

	async loginAndFetch(instance: string) {
		await this.apiModel.login(instance);
	}

	async fetchMoved(onLoadedBatch: (accounts: Array<AccountPair>) => void) {
		const accountData = (await this.apiModel.apiFetch("api/v1/accounts/verify_credentials", "GET")).json;
		console.log("account data", accountData);
		let maxId: string | null = null;
		let total = 0;
		for (; ;) {
			const query: Record<string, string | number> = { limit: 80 };
			if (maxId != null) {
				query["max_id"] = maxId;
			}
			console.log("fetching with max_id", maxId);
			const followingBatch = await this.apiModel.apiFetch(`api/v1/accounts/${accountData.id}/following`, "GET", query);
			const loadedAccounts: Array<Account> = followingBatch.json;
			total += loadedAccounts.length;
			console.log(`fetched ${loadedAccounts.length} accounts, total ${total}`);
			const moved = await this.intoPairs(loadedAccounts)
			onLoadedBatch(moved)

			if (loadedAccounts.length == 0) {
				break;
			}
			const relHeader = followingBatch.headers.get("Link");
			if (relHeader == null) {
				console.log("Link is null");
				break;
			};
			const rel = parseRelHeader(relHeader);
			const next = rel.get("next");
			if (next == null)
				break;
			const nextUrl = new URL(next);
			maxId = nextUrl.searchParams.get("max_id");
		}
	}

	async fetchFollowing(onLoadedBatch: (accounts: Array<Account>) => void) {
		const accountData = (await this.apiModel.apiFetch("api/v1/accounts/verify_credentials", "GET")).json;
		console.log("account data", accountData);
		let maxId: string | null = null;
		let total = 0;
		for (; ;) {
			const query: Record<string, string | number> = { limit: 80 };
			if (maxId != null) {
				query["max_id"] = maxId;
			}
			console.log("fetching with max_id", maxId);
			const followingBatch = await this.apiModel.apiFetch(`api/v1/accounts/${accountData.id}/following`, "GET", query);
			const loadedAccounts: Array<Account> = followingBatch.json;
			total += loadedAccounts.length;
			console.log(`fetched ${loadedAccounts.length} accounts, total ${total}`);
			onLoadedBatch(loadedAccounts)

			if (loadedAccounts.length == 0) {
				break;
			}
			const relHeader = followingBatch.headers.get("Link");
			if (relHeader == null) {
				console.log("Link is null");
				break;
			};
			const rel = parseRelHeader(relHeader);
			const next = rel.get("next");
			if (next == null)
				break;
			const nextUrl = new URL(next);
			maxId = nextUrl.searchParams.get("max_id");
		}
	}

	async fetchFollowers(onLoadedBatch: (accounts: Array<Account>) => void): Promise<void> {
		const accountData = (await this.apiModel.apiFetch("api/v1/accounts/verify_credentials", "GET")).json;
		console.log("account data", accountData);
		let maxId: string | null = null;
		let total = 0;
		for (; ;) {
			const query: Record<string, string | number> = { limit: 80 };
			if (maxId != null) {
				query["max_id"] = maxId;
			}
			console.log("fetching with max_id", maxId);
			const followingBatch = await this.apiModel.apiFetch(`api/v1/accounts/${accountData.id}/followers`, "GET", query);
			const loadedAccounts: Array<Account> = followingBatch.json;
			total += loadedAccounts.length;
			console.log(`fetched ${loadedAccounts.length} accounts, total ${total}`);
			onLoadedBatch(loadedAccounts)

			if (loadedAccounts.length == 0) {
				break;
			}
			const relHeader = followingBatch.headers.get("Link");
			if (relHeader == null) {
				console.log("Link is null");
				break;
			};
			const rel = parseRelHeader(relHeader);
			const next = rel.get("next");
			if (next == null) {
				console.log("next is null", relHeader);
				break;
			}
			const nextUrl = new URL(next);
			maxId = nextUrl.searchParams.get("max_id");
		}
	}

	async onfolllow(newAccount: Account): Promise<Relationship> {
		console.log("following", newAccount.acct);;
		try {
			const r = await this.apiModel.apiFetch(`/api/v1/accounts/${newAccount.id}/follow`, "POST");
			console.log("followed", newAccount.acct);
			return r.json
		} catch (e) {
			console.error("follow failed", e);
			throw e;
		}
	}

	async onUnfollow(account: Account): Promise<Relationship> {
		console.log("unfollowing", account.acct);
		try {
			const r = await this.apiModel.apiFetch(`/api/v1/accounts/${account.id}/unfollow`, "POST");
			console.log("unfollowed", account.acct);
			return r.json;
		} catch (e) {
			console.error("unfollow failed", e);
			throw e;
		}
	}

	async onRemoveFromFollowers(account: Account): Promise<Relationship> {
		console.log("blocking", account.acct);
		try {
			await this.apiModel.apiFetch(`/api/v1/accounts/${account.id}/block`, "POST");
			console.log("blocked", account.acct);
			const r = await this.apiModel.apiFetch(`/api/v1/accounts/${account.id}/unblock`, "POST");
			console.log("unblocked", account.acct);
			return r.json;
		} catch (e) {
			console.error("block/unblock failed", e);
			throw e;
		}
	}

	private async intoPairs(accounts: Array<Account>): Promise<Array<AccountPair>> {
		const result = []
		for (const account of accounts) {
			if (account.moved) {
				const [oldAccount, newAccount] = await this.getRelationships([account, account.moved]);
				result.push({ oldAccount, newAccount });
			}
		}
		return result
	}

	private async getRelationships(accounts: Array<Account>): Promise<Array<RelatedAccount>> {
		const ids: Array<number> = accounts.map(a => a.id);
		const relationships = (await this.apiModel.apiFetch(`api/v1/accounts/relationships`, "GET", { id: ids })).json

		return accounts.map((account, i) => {
			return { account, relationship: relationships[i] }
		})
	}
}

function parseRelHeader(header: string) {
	const parts = header.split(",");
	const result = new Map()
	for (const part of parts) {
		const [urlPart, relPart] = part.split(";")
		const [relKey, relValue] = relPart.split("=");
		const rel = relValue.replace(/\s/g, "").slice(1, -1);
		const url = urlPart.replace(/\s/g, "").slice(1, -1);
		result.set(rel, url);
	}
	return result;
}