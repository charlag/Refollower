import { h } from "preact";
import { useEffect, useReducer, useRef, useState } from "preact/hooks";
import { AccountInfo } from "./AccountInfo";
import type { Account } from "./ApiModel";
import type { Controller } from "./Controller";
import { DatePicker } from "./DatePicker";
import { ProgressIndicator } from "./ProgressIndicator";
import { showSnackbar } from "./Snackbar";

type Props = {
    controller: Controller
}

type Action =
    | { type: "accountsLoaded", accounts: Array<Account> }
    | { type: "unfollowed", accountId: number }

function accountsReducer(state: Array<Account>, action: Action) {
    switch (action.type) {
        case "accountsLoaded":
            return state.concat(action.accounts);
        case "unfollowed":
            return state.filter(a => a.id !== action.accountId)
        default:
            throw new Error("Unknown action " + action);
    }
}

export function FollowingAccountList({ controller }: Props) {
    const [inactiveSince, setInactiveSince] = useState(() => {
        const d = new Date();
        d.setHours(0)
        d.setMinutes(0);
        d.setSeconds(0);
        d.setMilliseconds(0);
        return d;
    })
    const [isLoading, setLoading] = useState(false);
    const [accounts, dispatch] = useReducer(accountsReducer, [])

    useEffect(() => {
        setLoading(true)
        controller.fetchFollowing((batch) => dispatch({ type: "accountsLoaded", accounts: batch }))
            .finally(() => setLoading(false));
    }, []);

    async function unfollow(e: Event, account: Account) {
        e.stopPropagation();
        try {
            await controller.onUnfollow(account)
            dispatch({ type: "unfollowed", accountId: account.id })
            showSnackbar("Unfollowed " + account.acct)
        } catch (e) {
            showSnackbar(e.message);
        }
    }

    return <div className="account-list">
        <ProgressIndicator loading={isLoading} />
        <h2>Following accounts inactive since {inactiveSince.toLocaleDateString()}:</h2>
        <div style="margin-bottom: 8px;">
            <DatePicker date={inactiveSince} onDateSelected={setInactiveSince} />
        </div>
        {
            accounts.map(a => {
                const d = new Date(a.last_status_at)
                if (d > inactiveSince) {
                    return null;
                }
                return <div className="item" key={a.id}>
                    <AccountInfo account={a}>
                        <div className="following-items">
                            <div className="following-active-date">{a.last_status_at == null ? "--" : d.toLocaleDateString()}</div>
                            <button className="text-button" onClick={(e) => { unfollow(e, a) }}>Unfollow</button>
                        </div>
                    </AccountInfo>
                </div>
            })
        }
    </div>
}