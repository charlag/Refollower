import { Controller } from "./Controller";
import { h, render } from "preact";
import { App } from "./App";
import { ObjectStorage } from "./ObjectStorage";

const objectStorage = new ObjectStorage();
const controller = new Controller(objectStorage);

const root = document.getElementById("root") as HTMLElement;

render(h(App, {objectStorage, controller}), root);

export default {}