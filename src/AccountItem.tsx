import { h, RenderableProps } from "preact";
import { useCallback, useMemo } from "preact/hooks";
import { AccountInfo } from "./AccountInfo";
import type { Account } from "./ApiModel";
import type { AccountPair, RelatedAccount, Relationship } from "./Controller";

type Props = {
    account: AccountPair,
    onFollow: () => void,
    onUnfollow: () => void,
    onRemoveFromFollowers: () => void,
}

function stopPropagation(fn: () => void) {
    return useCallback((e: Event) => {
        e.stopPropagation();
        fn();
    }, [fn])
}

export function MovedAccountItem(props: Props): h.JSX.Element {
    const {oldAccount, newAccount} = props.account;

    const unfollow = stopPropagation(props.onUnfollow);
    const removeFromFollowers = stopPropagation(props.onRemoveFromFollowers)
    const follow = stopPropagation(props.onFollow)

    return <div className="item">
        <AccountInfo account={oldAccount.account} relationship={oldAccount.relationship}>
            <div className="expander"></div>
            <div className="item-buttons">
                <button className="text-button" onClick={unfollow} disabled={!oldAccount.relationship.following}>Unfollow</button>
                <button className="text-button" onClick={removeFromFollowers} disabled={!oldAccount.relationship.followed_by}>Remove from followers</button>
            </div>
        </AccountInfo>
        <div className="secondary">has moved to</div>
        <AccountInfo account={newAccount.account} relationship={newAccount.relationship}>
        <div className="expander"></div>
            <div className="item-buttons">
                <button className="text-button" onClick={follow} disabled={newAccount.relationship.following || newAccount.relationship.requested}>Follow</button>
            </div>
        </AccountInfo>
    </div>;
}