import { ComponentChildren, h, RenderableProps } from "preact";
import { useEffect, useReducer, useState } from "preact/hooks";
import { MovedAccountItem } from "./AccountItem";
import type { Account } from "./ApiModel";
import type { AccountPair, Controller, Relationship } from "./Controller";
import { ProgressIndicator } from "./ProgressIndicator";

type Props = {
    controller: Controller,
}

function accountsReducer(state: Array<AccountPair>, action: Action) {
    switch (action.type) {
        case 'addAccounts': return state.concat(action.accounts);
        case 'relationshipUpdated': {
            return state.map(pair => {
                if (pair.oldAccount.account.id === action.accountId) {
                    return {
                        oldAccount: { account: pair.oldAccount.account, relationship: action.relationship },
                        newAccount: pair.newAccount
                    }
                } else if (pair.newAccount.account.id === action.accountId) {
                    return {
                        oldAccount: pair.oldAccount,
                        newAccount: { account: pair.newAccount.account, relationship: action.relationship }
                    }
                } else {
                    return pair
                }
            })
        }
        default: throw new Error("Unexpected action")
    }
}

type Action =
    | { type: "addAccounts", accounts: Array<AccountPair> }
    | { type: "relationshipUpdated", accountId: number, relationship: Relationship }

export function MovedAccountList(
    props: Props
) {
    const [isLoading, setLoading] = useState(false);
    const [accounts, dispatch] = useReducer(accountsReducer, [])
    useEffect(() => {
        setLoading(true);
        props.controller.fetchMoved((accounts) => { dispatch({ type: "addAccounts", accounts }) })
            .finally(() => setLoading(false));
    }, []);

    async function onFollow(account: Account) {
        const relationship = await props.controller.onfolllow(account)
        dispatch({ type: "relationshipUpdated", accountId: account.id, relationship })
    }

    async function onUnFollow(account: Account) {
        const relationship = await props.controller.onUnfollow(account)
        dispatch({ type: "relationshipUpdated", accountId: account.id, relationship })
    }

    async function onRemoveFromFollowers(account: Account) {
        const relationship = await props.controller.onRemoveFromFollowers(account)
        dispatch({ type: "relationshipUpdated", accountId: account.id, relationship })
    }

    return (
        <div className="account-list">
            <ProgressIndicator loading={isLoading} />
            <h2>Moved accounts:</h2>
            {
                accounts.map(account => {
                    return (
                        <MovedAccountItem
                            account={account}
                            onFollow={() => onFollow(account.newAccount.account)}
                            onUnfollow={() => onUnFollow(account.oldAccount.account)}
                            onRemoveFromFollowers={() => onRemoveFromFollowers(account.oldAccount.account)}
                        />
                    )
                })
            }
        </div>
    )
}