import { h } from "preact";
import { useCallback, useEffect, useReducer, useState } from "preact/hooks";
import { MovedAccountList } from "./MovedAccountList";
import type { Account } from "./ApiModel";
import type { AccountPair, Controller, RelatedAccount, Relationship } from "./Controller";
import { LoginForm } from "./LoginForm";
import { FollowingAccountList } from "./FollowingAccountList";
import { FollowerAccountList } from "./FollowerAccountList";
import type { ObjectStorage } from "./ObjectStorage";

type Props = { 
    controller: Controller,
    objectStorage: ObjectStorage,
 }

type State =
    | { type: "notLoggedIn", loading: boolean }
    | { type: "chooser" }
    | { type: "moved" }
    | { type: "following", inactiveSince: Date }
    | { type: "followers", inactiveSince: Date }

export function App(props: Props) {
    const [state, setState] = useState<State>({ type: "notLoggedIn", loading: false })

    async function loginAndLoad(instance: string) {
        setState({ type: "notLoggedIn", loading: true });
        try {
            await props.controller.loginAndFetch(instance);
            setState({ type: "chooser" });
        } catch {
            setState({ type: "notLoggedIn", loading: false });
        }
    }

    useEffect(() => {
        setState({ type: "notLoggedIn", loading: true });
        props.controller.handleOauthRedirect()
            .then((instance) => {
                if (instance) {
                    loginAndLoad(instance)
                } else {
                    setState({ type: "notLoggedIn", loading: false });
                }
            })
    }, [])


    function renderInner() {
        switch (state.type) {
            case "notLoggedIn":
                return (
                    <div>
                        <div className="indefinite-progress" style={state.loading ? "" : "opacity: 0;"}></div>
                        <LoginForm objectStorage={props.objectStorage} onLogin={loginAndLoad} />
                    </div>
                );
            case "chooser":
                return renderChooser();
            case "moved":
                return renderList();
            case "following":
                return renderFollowingAccountList(state);
                case "followers":
                    return renderFollowersAccountList(state);
            default:
                throw new Error("unknown state");
        }
    }

    function renderList() {
        return <MovedAccountList
            controller={props.controller}
        />;
    }

    function renderFollowingAccountList(state: {inactiveSince: Date}) {
        return <FollowingAccountList controller={props.controller} inactiveSince={state.inactiveSince}></FollowingAccountList>
    }

    function renderFollowersAccountList(state: {inactiveSince: Date}) {
        return <FollowerAccountList controller={props.controller} inactiveSince={state.inactiveSince}></FollowerAccountList>
    }

    async function openMoved() {
        setState({ type: "moved" });
    }

    function renderChooser() {
        return <div className="chooser">
            <button className="button" onClick={openMoved}>Moved</button>
            <button className="button" onClick={() => setState({ type: "following", inactiveSince: new Date("2021-1-1") })}>Following</button>
            <button className="button" onClick={() => setState({ type: "followers", inactiveSince: new Date("2021-1-1") })}>Followers</button>
        </div>
    }

    return (
        <div className="App">
            {renderInner()}
        </div>
    )
}